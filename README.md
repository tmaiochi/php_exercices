CI&T PHP Exercises
===============

### Get the repository
- Download or clone the repository

### Run composer install
- Composer install

### Start lando
-  lando start

### Run the tests
-  lando test tests/ClassNameTest.php
