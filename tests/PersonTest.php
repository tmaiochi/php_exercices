<?php

use PHPUnit\Framework\TestCase;

/**
 * Class to do some tests to Book class.
 */
class PersonTest extends TestCase {

  /**
   * Set some var to start tests.
   */
  public function setUp(): void {
    $this->Person = new Person("John Doe", "Male", 37);
  }

  /**
   * Test to constructors.
   */
  public function testConstructor() {
    // $this->Person = new Person("John Doe", 37, "Male");
    $this->assertEquals("John Doe", $this->Person->getName());
    $this->assertEquals(37, $this->Person->getAge());
    $this->assertEquals("Male", $this->Person->getGenre());
  }

  /**
   * Test function birthday.
   */
  public function testBirthday() {
    $this->assertEquals(($this->Person->getAge() + 1), $this->Person->birthday());
  }

}
