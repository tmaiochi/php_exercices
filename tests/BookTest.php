<?php

use PHPUnit\Framework\TestCase;

/**
 * Class to do some tests to Book class.
 */
class BookTest extends TestCase {

  /**
   * Set some var to start tests.
   */
  public function setUp(): void {
    $this->Person = new Person("John Doe", "Male", 37);
    $this->book = new Book("O Hobbit", "J.R.R Tolkien", 300, $this->Person);
    $this->book2 = new Book("O Senhor dos Aneis - O Retorno do Rei", "J.R.R Tolkien", 527, $this->Person);
  }

  /**
   * Test to constructors.
   */
  public function testConstructor() {
    $this->assertEquals("O Hobbit", $this->book->getTitle());
    $this->assertEquals("J.R.R Tolkien", $this->book->getAuthor());
    $this->assertEquals(300, $this->book->getTotalPages());
    $this->assertEquals("John Doe", $this->Person->getName());
  }

  /**
   * Test to function open.
   */
  public function testOpen() {
    $this->assertEquals(TRUE, $this->book->open());
  }

  /**
   * Test to function close.
   */
  public function testClose() {
    $this->assertEquals(FALSE, $this->book->close());
  }

  /**
   * Test to function browse.
   */
  public function testBrowse() {
    $this->assertEquals(0, $this->book->browse(301));
    $this->assertEquals(100, $this->book->browse(100));
    $this->assertEquals(0, $this->book->browse(-1));

  }

  /**
   * Test to function jumpFoward.
   */
  public function testJumpFoward() {
    $this->book->setPage(300);
    $this->assertEquals("You finish this book!", $this->book->jumpToPageFoward());
    $this->book2->setPage($this->book->getTotalPages() - 5);
    $this->assertEquals(($this->book2->getPage() + 1), $this->book2->jumpToPageFoward());
  }

  /**
   * Test to function jumBackWard.
   */
  public function testJumpBackWard() {
    $this->book->setPage(1);
    $this->assertEquals("You are in the beginning of the book", $this->book->jumpToPageBack());
    $this->book2->setPage(200);
    $this->assertEquals(($this->book2->getPage() - 1), $this->book2->jumpToPageBack());

  }

}
