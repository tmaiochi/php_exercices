<?php

/**
 * Validade the social number.
 *
 * @inheritDoc
 */
class SocialNumber {

  /**
   * Return a type of a triangle.
   *
   * @param string $socialNumber
   *   Receive a string with the social number do be validated.
   *
   * @return bool
   *   Return a bool with the validation
   */
  public static function validateSocialNumber(string $socialNumber):bool {
    // Place your code here.
    if (strlen($socialNumber) != 11 || preg_match('/([0-9])\1{10}/', $socialNumber)) {
      return FALSE;
    }

    $sum = 0;
    $socialNumberIndex = 0;
    $multiplicate = 10;

    while (($multiplicate >= 2) && ($socialNumberIndex < 9)) {
      $sum += $socialNumber[$socialNumberIndex] * ($multiplicate);

      $multiplicate--;
      $socialNumberIndex++;
    }

    $rest1 = (($sum * 10) % 11);

    if ($rest1 == 10) {
      $rest1 = 0;
    }

    $sum = 0;
    $socialNumberIndex = 0;
    $multiplicate = 11;

    while (($multiplicate >= 2) && ($socialNumberIndex < 10)) {
      $sum += $socialNumber[$socialNumberIndex] * ($multiplicate);

      $multiplicate--;
      $socialNumberIndex++;
    }

    $rest2 = (($sum * 10) % 11);

    if ($rest2 == 10) {
      $rest2 = 0;
    }

    if ($rest1 == $socialNumber[9] && $rest2 == $socialNumber[10]) {
      return TRUE;
    }

    return FALSE;
  }

}
