<?php

/**
 * @file
 * File for tests.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP Exercice</title>
</head>
<body>

    <div>
        <?php
        require_once 'Person.php';
        require_once 'Book.php';

        $people1 = new Person("Thiago", "Masculino", 24);
        $people2 = new Person("Larissa", "Feminino", 23);
        $books1 = new Book("Harry Potter e a Pedra Filosofal", "J.K. Rowling", 208, $people1);
        $books2 = new Book("O Pequeno Principe", "Antoine de Saint-Exupéry", 98, $people2);
        $books3 = new Book("O Ladrão de Raios - Volume 1. Série Percy Jackson e os Olimpianos", "Rick Riordan", 400, $people1);


        $people1->birthday();
        $people1->getAge();
        $books1->open();
        $books1->browse(200);
        $books1->jumpToPageBack();
        $books2->browse(20);
        $books2->jumpToPageFoward();

        $books1->details();
        $books2->details();
        $books3->details();
        ?>
    </div>
    
</body>
</html>
