<?php

require_once 'Person.php';
require_once 'PublicationInterface.php';
/**
 * Class about Book.
 */
class Book implements PublicationInterface {
  /**
   * Title of the book.
   *
   * @var string
   */
  private $title;
  /**
   * Author Name.
   *
   * @var [type]
   */
  private $author;
  /**
   * Number of pages in the book.
   *
   * @var int
   */
  private $totalPages;
  /**
   * Current page of the reader.
   *
   * @var int
   */
  private $page = 0;
  /**
   * Show if book is open or close.
   *
   * @var bool
   */
  private $open = FALSE;
  /**
   * Info about who is reading this book.
   *
   * @var Person
   */
  private $reader;

  /**
   * Constructor of the class, set some importants infos.
   *
   * @param string $title
   *   Name of the book.
   * @param string $author
   *   Name of the author.
   * @param int $totalPages
   *   Number of pages in book.
   * @param Person $reader
   *   Who is reading this book.
   */
  public function __construct($title, $author, $totalPages, Person $reader) {
    // Place your code here.
    $this->title = $title;
    $this->author = $author;
    $this->totalPages = $totalPages;
    $this->reader = $reader;
  }

  /**
   * Show info about book.
   */
  public function details() {
    // Place your code here.
    echo '<span><b>Book Title:</b> ' . $this->title . '</span>';
    echo '<br><span><b>Author:</b> ' . $this->author . '</span>';
    echo '<br><span><b>Current Page:</b> ' . $this->page . '</span>';
    echo '<br><span><b>Reader By:</b> ' . $this->reader->getName() . '<br> <b>Age:</b> ' . $this->reader->getAge() . '</span>';
    echo '<hr/>';
  }

  /**
   * Get the title.
   *
   * @return string
   *   Return the title.
   */
  public function getTitle() {
    // Place your code here.
    return $this->title;
  }

  /**
   * Get the author.
   *
   * @return string
   *   Return the author.
   */
  public function getAuthor() {
    // Place your code here.
    return $this->author;
  }

  /**
   * Get total pages.
   *
   * @return int
   *   Return number of pages.
   */
  public function getTotalPages() {
    // Place your code here.
    return $this->totalPages;
  }

  /**
   * Get current page.
   *
   * @return int
   *   Return current page.
   */
  public function getPage() {
    // Place your code here.
    return $this->page;
  }

  /**
   * Get reader.
   *
   * @return Person
   *   Return the reader.
   */
  public function getReader() {
    // Place your code here.
    return $this->reader;
  }

  /**
   * Set the author's name.
   *
   * @param string $author
   *   Author's name.
   */
  public function setAuthor($author) {
    // Place your code here.
    $this->author = $author;
  }

  /**
   * Set the title.
   *
   * @param string $title
   *   Book's title.
   */
  public function setTitle($title) {
    // Place your code here.
    $this->title = $title;
  }

  /**
   * Set the number of pages.
   *
   * @param int $totalPages
   *   Total page number.
   */
  public function setTotalPages($totalPages) {
    // Place your code here.
    $this->totalPages = $totalPages;
  }

  /**
   * Set the current page.
   *
   * @param int $page
   *   Page number.
   */
  public function setPage($page) {
    // Place your code here.
    $this->page = $page;
  }

  /**
   * Set that the book is open.
   *
   * @return bool
   *   return if book is open.
   */
  public function open() {
    // Place your code here.
    return $this->open = TRUE;
  }

  /**
   * Set that the book is closed.
   *
   * @return bool
   *   return if book is open.
   */
  public function close() {
    // Place your code here.
    return $this->open = FALSE;
  }

  /**
   * Open the book on a specific page.
   *
   * @param int $page
   *   Page number.
   *
   * @return int
   *   Return current page number.
   */
  public function browse($page) {
    // Place your code here.
    if ($page > $this->totalPages || $page < 0) {
      $this->page = 0;
    }
    else {
      $this->page = $page;
    }

    return $this->page;
  }

  /**
   * Move to the next page.
   */
  public function jumpToPageFoward() {
    // Place your code here.
    if (($this->getPage() + 1) > $this->totalPages) {
      return 'You finish this book!';
    }
    else {
      return $this->page = $this->page + 1;
    }
  }

  /**
   * Move to the previous page.
   */
  public function jumpToPageBack() {
    // Place your code here.
    if (($this->getPage() - 1) <= 0) {
      return 'You are in the beginning of the book';
    }
    else {
      return $this->page = $this->page - 1;
    }
  }

}
