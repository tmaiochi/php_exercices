<?php

/**
 * Class about Person.
 */
class Person {

  /**
   * Person's name.
   *
   * @var string
   */
  private $name;
  /**
   * Person's genre.
   *
   * @var string
   */
  private $genre;
  /**
   * Person's age.
   *
   * @var int
   */
  private $age;

  /**
   * Constructor of the class, set some importants infos.
   *
   * @param string $name
   *   Person's name.
   * @param string $genre
   *   Person's genre.
   * @param int $age
   *   Person's age.
   */
  public function __construct($name, $genre, $age) {
    $this->name = $name;
    $this->genre = $genre;
    $this->age = $age;
  }

  /**
   * Get person's name.
   *
   * @return string
   *   Return person's name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get person's age.
   *
   * @return int
   *   Return person's age.
   */
  public function getAge() {
    return $this->age;
  }

  /**
   * Get person's genre.
   *
   * @return string
   *   Return person's genre.
   */
  public function getGenre() {
    return $this->genre;
  }

  /**
   * Set person's name.
   *
   * @param string $name
   *   Person's name.
   */
  public function setName($name) {
    // Place your code here.
    $this->name = $name;
  }

  /**
   * Set person's age.
   *
   * @param int $age
   *   Person's age.
   */
  public function setAge($age) {
    $this->age = $age;
  }

  /**
   * Set person's genre.
   *
   * @param string $genre
   *   Person's genre.
   */
  public function setGenre($genre) {
    // Place your code here.
    $this->genre = $genre;
  }

  /**
   * Change person's age.
   *
   * @return int
   *   Return new age.
   */
  public function birthday() {
    // Place your code here.
    return $this->age = $this->age + 1;
  }

}
