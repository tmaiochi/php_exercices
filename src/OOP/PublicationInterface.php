<?php

/**
 * An interface for publication of books.
 */
interface PublicationInterface {

  /**
   * Sets the book is open.
   */
  public function open();

  /**
   * Sets the book is open.
   */
  public function close();

  /**
   * Move to the next page.
   */
  public function jumpToPageFoward();

  /**
   * Move to the previous page.
   */
  public function jumpToPageBack();

}
