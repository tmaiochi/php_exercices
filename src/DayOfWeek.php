<?php

/**
 * Return a day of the week of a given interger.
 *
 * @inheritDoc
 */
class DayOfWeek {

  /**
   * Return the day of the week.
   *
   * @param int $number
   *   Receive a interger.
   *
   * @return string
   *   Return a day of the week
   */
  public static function whatDayOfWeek($number): String {
    // Place your code here.
    switch ($number) {
      case 1:
        return "Sunday";

      case 2:
        return "Monday";

      case 3:
        return "Tuesday";

      case 4:
        return "Wednesday";

      case 5:
        return "Thursday";

      case 6:
        return "Friday";

      case 7:
        return "Saturday";

      default:
        return "An invalid value was entered.";
    }

  }

}
